#include <iostream>
#include <ostream>
#include <cassert>

struct Rational {
	unsigned long long const dividend, divisor;
	bool const negative;

	constexpr Rational (unsigned long long const dividend):
		dividend{dividend}, divisor{1}, negative{false}
		{}

	constexpr Rational operator - (void) const
		{ return {dividend, divisor, !negative}; }

	Rational operator / (long long newdivisor) const {
		assert(divisor == 1ul);
		bool neg = negative;
		if (newdivisor < 0) {
			neg = !neg;
			newdivisor = -newdivisor;
		}
		assert(newdivisor > 0);
		return {dividend, static_cast<unsigned long long int const>(newdivisor), neg};
	}

private:
	constexpr Rational (unsigned long long const dividend, unsigned long long const divisor, bool const negative):
		dividend{dividend}, divisor{divisor}, negative{negative}
		{}
	Rational (Rational const&) = delete;
	Rational (Rational&&) = delete;
	void operator = (Rational const&) = delete;
	void operator = (Rational&&) = delete;
};

static std::ostream& operator << (std::ostream& o, Rational const& r) {
	if (r.negative)
		o << '-';
	return o << r.dividend << '/' << r.divisor;
}

static constexpr Rational operator "" _r (unsigned long long dividend)
	{ return {dividend}; }


////////////////////////////////////////

struct Id {
	const std::string id;
	Id (std::string const& id): id{id} {}
	Id (std::string&& str): id(std::move(str)) {}
private:
	Id (Id const&) = delete;
	Id (Id&&) = delete;
	void operator = (Id const&) = delete;
	void operator = (Id&&) = delete;
};

static Id operator "" _id (char const* const id)
	{ return {id}; }

////////////////////////////////////////

int main (int, char *[]) {
	std::cout << -34_r / -12 << std::endl;
}
