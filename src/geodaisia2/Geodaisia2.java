package geodaisia2;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Path;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import static geodaisia2.Collections.flattenIterables;
import static geodaisia2.Constants.ten;
import static geodaisia2.Constants.utf8;
import static geodaisia2.IntermediatePointsGenerator.colouriseLines;
import static geodaisia2.IntermediatePointsGenerator.generateAllIntermediateHeightPoints;
import static geodaisia2.IntermediatePointsGenerator.generateHeightLines2;
import static geodaisia2.IntermediatePointsGenerator.colourisePoints;
import static geodaisia2.Numbers.interpolate;
import static geodaisia2.Persistance.loadPoints;
import static geodaisia2.Renderer.writeLinesToSvg;
import static geodaisia2.Renderer.writePointsToSvg;
import static geodaisia2.Renderer.writeSvgFooter;
import static geodaisia2.Renderer.writeSvgHeader;
import static java.lang.System.out;
import static java.nio.file.Files.newOutputStream;
import static java.nio.file.Paths.get;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;
import static org.jscience.mathematics.number.LargeInteger.ONE;
import static org.jscience.mathematics.number.LargeInteger.ZERO;
import static org.jscience.mathematics.number.Rational.valueOf;

public class Geodaisia2 {

	public static void main (final String[] args) throws IOException {
		final Path poth = get("tostsvg.svgz");
		try (
				final OutputStream outs = newOutputStream(poth, TRUNCATE_EXISTING, WRITE, CREATE);
				final BufferedOutputStream bouts = new BufferedOutputStream(outs, 10240);
				final GZIPOutputStream gzouts = new GZIPOutputStream(bouts, 10240, true);
				final OutputStreamWriter w = new OutputStreamWriter(gzouts, utf8);
		) {
			final List<? extends List<? extends DeclaredPoint>> points = loadPoints();
			out.println("points loaded");
			final List<? extends List<? extends List<? extends GeneratedPoint>>> intermediates = generateAllIntermediateHeightPoints(points);
			out.println("intermediates generated");
			writeSvgHeader(w);
//			writeLinesToSvg(w, colouriseLines(generateHeightLines2(intermediates, 30)));
			out.println("level lines generated, colourised and written");
			writePointsToSvg(w, flattenIterables(points), colourisePoints(flattenIterables(flattenIterables(intermediates))));
			out.println("intermediate points colourised and written");
			writeSvgFooter(w);

			out.printf("written %s%n", poth.toAbsolutePath());
		}

		out.println(interpolate(ZERO, ten, valueOf(1l, 5l)));
		out.println(interpolate(ONE, ten, valueOf(1l, 5l)));
	}

	private Geodaisia2 () {
	}
}
