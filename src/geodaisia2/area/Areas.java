package geodaisia2.area;

import geodaisia2.GeneratedPoint;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.jscience.mathematics.number.Real;

import static geodaisia2.Geometry.area;
import static geodaisia2.Constants.utf8;

public class Areas {

	public static void main (final String[] args) throws IOException {
		final StringBuilder bob = new StringBuilder(40960);
		for (final String line: Files.readAllLines(Paths.get("material/areas.txt"), utf8))
			bob.append(line);

		for (final String polygonspec: bob.toString().split("\\|")) {
			final List<GeneratedPoint> points = new LinkedList<>();
			for (final String pointspec: polygonspec.split("/")) {
				final String[] coords = pointspec.split("\\.");
				final long	x = Long.parseLong(coords[0].trim()),
							y = Long.parseLong(coords[1].trim());
				points.add(GeneratedPoint.create(x, y, 0l));
			}
			final Collection<Real> sides = new LinkedList<>();
			System.out.println(area(sides, points));
			System.out.println(sides);
		}
	}
}
