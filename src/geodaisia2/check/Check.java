package geodaisia2.check;

import java.io.PrintWriter;
import org.jscience.mathematics.number.Rational;
import org.jscience.mathematics.number.Real;

import static geodaisia2.Numbers.*;

class Check {
	private final Rational
			x_k1 = Rational.valueOf(-147347l, 100l),
			x_k2 = Rational.valueOf(941144l, 1000l),
			x_k3 = Rational.valueOf(438873l, 1000l),
			y_k1 = Rational.valueOf(-86497l, 100l),
			y_k2 = Rational.valueOf(135878l, 100l),
			y_k3 = Rational.valueOf(-99917l, 100l),

			tan_a_k2k1 = distance(x_k1, x_k2).divide(distance(y_k1, y_k2)),

			tan_a_k2k3 = distance(x_k3, x_k2).divide(distance(y_k3, y_k2));
	private final double
			a_k2k1 = togon(Math.atan(tan_a_k2k1.doubleValue())),
			a_k2k3 = togon(Math.atan(tan_a_k2k3.doubleValue()));

	public void write (final PrintWriter w) {
		w.printf(""
				+ "x_k1: %1$s%n"
				+ "x_k2: %2$s%n"
				+ "x_k3: %3$s%n"
				+ "y_k1: %4$s%n"
				+ "y_k2: %5$s%n"
				+ "y_k3: %6$s%n"
				+ "%n"
				+ "tan a_k1k2: %7$s%n"
				+ "a_k2k1: %8$s%n"
				+ "%n"
				+ "tan a_k2k3: %9$s%n"
				+ "a_k2k3: %10$s%n"
				+ "",
				toReal(x_k1),			// 1
				toReal(x_k2),			// 2
				toReal(x_k3),			// 3
				toReal(y_k1),			// 4
				toReal(y_k2),			// 5
				toReal(y_k3),			// 6
				toReal(tan_a_k2k1),		// 7
				a_k2k1,					// 8
				toReal(tan_a_k2k3),		// 9
				a_k2k3,					// 10
				null
				);
	}

	private static double fromgon (final double v) {
		return v * Math.PI / 200.0;
	}

	private static double togon (final double rad) {
		return rad * Math.PI / 200.0;
	}
}
